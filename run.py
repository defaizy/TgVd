#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import telebot
import yt_dlp
import json
import os

#  токен для бота
bot = telebot.TeleBot('pasteyourtokenhere')

#  /start
@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, '🇷🇺: Пришлите ссылку типа "https://ссылка".\n🇬🇧: Share a link like "https://link".')

#  получение ссылки на видео от пользователя
@bot.message_handler(content_types=['text'])
def download(message):

        #  настройки для yt-dlp
        ydl_opts = {
            "format": "mp4",
            "outtmpl": "videos/"+str(message.chat.id) + ".mp4",
            "playlistend": 1,
            "nooverwrites": False,
            "quiet": True
        }

        msg = bot.send_message(message.chat.id, '🇷🇺: Скачиваю ваше видео...\n🇬🇧: Downloading your video...')

        #  скачивание видео
        try:
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                ydl.download([message.text])

            #  отправка скачанного видео
            video = open("videos/"+str(message.chat.id) + ".mp4", 'rb')
            try:
                bot.edit_message_text(chat_id = msg.chat.id, message_id = msg.message_id, text='🇷🇺: Отправляю вам ваше видео...\n🇬🇧: Sending your video...')
                bot.send_video(message.chat.id, video)
                bot.delete_message(chat_id = msg.chat.id, message_id = msg.message_id)
                ## удаление видео с нашего диска
                os.remove("videos/"+str(message.chat.id)+".mp4")
            except:
                bot.edit_message_text(chat_id = msg.chat.id, message_id = msg.message_id, text='🇷🇺: Ваше видео слишком длинное!\n🇬🇧: Your video is too long!')
        except:
            bot.edit_message_text(chat_id = msg.chat.id, message_id = msg.message_id, text='🇷🇺: По вашей ссылке не было найдено видео. Обратите внимание, ваша ссылка должна начинаться с https:// или http://\n🇬🇧: No video was found on your link. Please note that your link must start with https:// or http://')

#  запускаем
bot.infinity_polling()
